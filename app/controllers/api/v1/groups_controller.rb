module Api
  module V1
    class GroupsController < Api::V1::ApplicationController

      def index
        render json: [], status: 200
      end
    end
  end
end