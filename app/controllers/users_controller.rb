class UsersController < ApplicationController
  before_action :authenticate_user!, unless: :verify_api

  layout "user"

  def after_sign_in_path_for(resource)
    user_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end

  def dashboard
    render 'users/dashboard'
  end

  def check_user_permissions
    fail(AccessDenied.new) unless UserPolicy.new(current_user).access_to_admin_panel?
  end
end