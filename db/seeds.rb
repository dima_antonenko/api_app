def create_user(id)
  user = User.new(
      :email                 => "user#{id}@test.ru",
      :password              => "12345",
      :password_confirmation => "12345"
  )
  user.save!
  user.encrypted_password="$2a$10$LhFI1e6PxdnfPCUHwPFSJenkIIkntkd9v4SQd.BvCuC8VtuHdAqUi"
  user.save
end

User.destroy_all
['', '1', '2', '3', '4', '5'].each {|id| create_user(id)}
puts "Пользователи добавлены \n \n"