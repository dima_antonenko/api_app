source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# main
ruby '2.6.2'
gem 'rails', '~> 5.2.3'

# server
gem 'bootsnap', '>= 1.1.0', require: false
gem 'puma', '~> 3.11'
gem 'spring'
gem 'spring-watcher-listen', '~> 2.0.0'
gem 'jbuilder', '~> 2.7'

# db
gem 'pg', '~> 0.18.4'

# assets
gem 'uglifier', '>= 1.3.0'
gem 'sass-rails', '~> 5.0'
gem 'jquery-rails', '~> 4.3', '>= 4.3.1'

# architecture
gem "pundit"
gem 'devise_token_auth', '~> 0.1.31'
gem 'omniauth'
gem 'rack-cors', :require => 'rack/cors'

# locale
gem 'russian', '~> 0.6.0'

# other
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'awesome_rails_console'
  gem 'hirb'
  gem 'hirb-unicode'
  gem 'pry-byebug'
  gem 'pry-stack_explorer'
  gem 'rspec-rails', '~> 3.8'
  gem 'faker'
  gem 'factory_bot_rails'
  gem 'database_cleaner'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem "awesome_print"
  gem 'meta_request'
  gem 'bullet'
  gem 'better_errors'
  gem 'binding_of_caller'
end